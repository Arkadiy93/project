var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var inject = require('gulp-inject');
var del = require('del');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var babel = require("gulp-babel");
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var babelify = require("babelify");
var sourcemaps = require('gulp-sourcemaps');
var connect = require('gulp-connect');
var proxy = require('http-proxy-middleware');
var gutil = require('gulp-util');
var ifProduction = gutil.env.production || false;
var config = require('./config.json');
var env = ifProduction ? 'production' : 'development';

gulp.task('default', ['build', 'watch', 'connect'], function () {
	require(config.srcPaths.server);
});

gulp.task('connect', function () {
	connect.server({
		root: (config[env].main),
		livereload: true,
		port: 5000,
		middleware: function (connect, opt) {
			return [
				proxy('/api', {
					target: 'http://localhost:3000',
					changeOrigin: true,
					ws: true
				})
			]
		}
	});
});

gulp.task('watch', function () {
	gulp.watch([config.srcPaths.watchStyles], ['app-styles']);
	gulp.watch([config.srcPaths.js], ['app-scripts']);
	gulp.watch(config.srcPaths.index, ['index']);
});

gulp.task('build', function (callback) {
	runSequence('clean',
		['app-styles', 'app-scripts', 'vendor-styles', 'vendor-scripts'],
		'index',
		callback);
});

gulp.task('clean', function () {
	return del([config[env].clean], {
		force: true
	});
});

gulp.task('app-styles', function () {
	return gulp.src(config.srcPaths.styles)
	.pipe(sass().on('error', sass.logError))
	.pipe(config[env].minify ? cleanCSS({
			compatibility: 'ie8'
		}) : gutil.noop())
	.pipe(config[env].concat ? concat('all.css') : gutil.noop())
	.pipe(gulp.dest(config[env].styles))
	.pipe(config[env].livereload ? connect.reload() : gutil.noop());
});

gulp.task('vendor-styles', function () {
	return gulp.src(config[env].vendorStyles)
	.pipe(config[env].concat ? concat('vendors.min.css') : gutil.noop())
	.pipe(config[env].minify ? cleanCSS({
			compatibility: 'ie8'
		}) : gutil.noop())
	.pipe(gulp.dest(config[env].styles));
});

gulp.task('app-scripts', function () {
	const bundler = browserify(config.srcPaths.mainJS, {
			debug: true
		})
		.transform(babelify.configure({
				presets: ["es2015"]
			}));

	return bundler.bundle()
	.pipe(source('app.min.js'))
	.pipe(buffer())
	.pipe(config[env].sourcemaps ? sourcemaps.init({
			loadMaps: true
		}) : gutil.noop())
	.pipe(uglify())
	.pipe(sourcemaps.write('./'))
	.pipe(gulp.dest(config[env].js))
	.pipe(config[env].livereload ? connect.reload() : gutil.noop());
});

gulp.task('vendor-scripts', function () {
	return gulp.src(config[env].vendorScripts)
	.pipe(config[env].concat ? concat('vendors.min.js') : gutil.noop())
	.pipe(config[env].minify ? uglify() : gutil.noop())
	.pipe(gulp.dest(config[env].js));
});

gulp.task('index', function () {
	return gulp.src(config.srcPaths.index)
	.pipe(inject(gulp.src(config[env].indexInject, {
				read: false
			}), {
			ignorePath: config[env].main
		}))
	.pipe(gulp.dest(config[env].main))
	.pipe(config[env].livereload ? connect.reload() : gutil.noop());
});
